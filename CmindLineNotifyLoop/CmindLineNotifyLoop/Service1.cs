﻿using CmindLineNotifyLoop.Models;
using CmindLineNotifyLoop.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace CmindLineNotifyLoop
{
    public partial class Service1 : ServiceBase
    {
        public string notifyUrl = "https://notify-api.line.me/api/notify";
        private VendorRepository vendorRepository = new VendorRepository(new CmindLineNotifyDBEntities());
        private ServiceRepository serviceRepository = new ServiceRepository(new CmindLineNotifyDBEntities());
        private NotifyRepository notifyRepository = new NotifyRepository(new CmindLineNotifyDBEntities());
        private InterlockRepository interlockRepository = new InterlockRepository(new CmindLineNotifyDBEntities());
        private SequenceRepository sequenceRepository = new SequenceRepository(new CmindLineNotifyDBEntities());

        //public Service1() : this(null, null, null, null, null) { }

        //public Service1(VendorRepository repo, ServiceRepository repo2, NotifyRepository repo3, InterlockRepository repo4, SequenceRepository repo5)
        //{
        //    InitializeComponent();
        //    vendorRepository = repo ?? new VendorRepository(new CmindLineNotifyDBEntities());
        //    serviceRepository = repo2 ?? new ServiceRepository(new CmindLineNotifyDBEntities());
        //    notifyRepository = repo3 ?? new NotifyRepository(new CmindLineNotifyDBEntities());
        //    interlockRepository = repo4 ?? new InterlockRepository(new CmindLineNotifyDBEntities());
        //    sequenceRepository = repo5 ?? new SequenceRepository(new CmindLineNotifyDBEntities());
        //}
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
            DateTime nowTime = DateTime.Now;
            #region 新增序列
            //1. 取得" 時間已超過 "，" 尚未發送 "且" 主服務為啟用狀態 "的訊息，並新增序列
            var activeServiceList = serviceRepository.Query(0, true, true).Select(q => q.ID).ToList();           
            var msgList = notifyRepository.GetAll()
                .Where(q => activeServiceList.Contains(q.ServiceID) && q.SendDate <= nowTime && q.IsSent == false)
                .OrderBy(q => q.ServiceID)
                .ThenBy(q => q.SendDate).ToList();

            if (msgList.Count() > 0)
            {
                foreach (var msg in msgList)
                {//訊息迴圈
                    if (msg.ServiceID == 0 || msg.ID == 0)
                    {//缺少必要參數，跳出當次訊息迴圈
                        continue;
                    }
                    var interlockList = interlockRepository.Query(msg.ServiceID).ToList();
                    foreach (var ilock in interlockList)
                    {//連動迴圈
                        if (ilock.ID == 0)
                        {//缺少必要參數，跳出當次連動迴圈
                            continue;
                        }
                        //驗證是否已在序列
                        if (!CheckSequence(msg.ServiceID, msg.ID, ilock.ID))
                        {
                            //新增發送序列
                            SendSequence sq = new SendSequence
                            {
                                ID = new Guid(),
                                ServiceID = msg.ServiceID,
                                NotifyID = msg.ID,
                                InterlockID = ilock.ID,
                                TokenKey = ilock.TokenKey,
                                CreateDate = DateTime.Now,
                                IsSent = false,
                            };
                            sequenceRepository.Insert(sq);
                        }
                    }
                    //更新該訊息為已寄送狀態
                    msg.IsSent = true;
                    notifyRepository.Update(msg);
                }
            }
            #endregion

            #region 發送訊息
            //2. 寄送未超過限制的訊息
            int acceptCount = 1000;
            var sequenceList = sequenceRepository.GetAll().OrderBy(q => q.CreateDate).ToList();
            if (sequenceList.Count() > 0)
            {
                //var limitStartTime = sequenceList.FirstOrDefault().CreateDate;
                //var limitStopTime = limitStartTime.AddHours(1);

                //計算1小時內已發送/占用額度的訊息數量
                var occupyTime = nowTime.AddHours(-1);
                var occupy = sequenceList.Where(q => q.CreateDate > occupyTime && q.IsSent == true).Count();
                int sendable = acceptCount - occupy;

                if (sendable > 0)
                {
                    var unSentList = sequenceList.Where(q => q.IsSent == false).OrderBy(q => q.CreateDate).Take(sendable);

                    foreach (var sq in unSentList)
                    {
                        //發送訊息
                        var msgInfo = notifyRepository.GetById(sq.NotifyID);
                        var result = SetNotify(sq.ServiceID, msgInfo.Message, msgInfo.ImageUrl, msgInfo.Sticker, sq.InterlockID, sq.TokenKey);
                        if (result == "success")
                        {//更新已發送的連動序列
                            sq.IsSent = true;
                            sequenceRepository.Update(sq);
                        }
                        if (result == "delete")
                        {//刪除已解除連動的序列
                            sequenceRepository.DeleteByGuid(sq.ID);
                        }
                    }
                }
            }
            #endregion

            #region 刪除已發送完畢的訊息
            //3. 刪除已發送完畢的訊息
            var sentList = sequenceRepository.Query(true).GroupBy(q => q.NotifyID).ToList();
            if (sentList.Count() > 0)
            {
                foreach (var item in sentList)
                {
                    var thisNotifySequence = sequenceRepository.Query(null, item.FirstOrDefault().ServiceID, item.FirstOrDefault().NotifyID);
                    if (thisNotifySequence.Where(q => q.IsSent == false).Count() == 0)
                    {
                        //刪除所有訊息皆已發送的序列
                        var notifySentList = thisNotifySequence.Where(q => q.IsSent == true);
                        foreach (var sq in notifySentList)
                        {
                            sequenceRepository.DeleteByGuid(sq.ID);
                        }
                    }
                }
            }
                #endregion

            }
            catch (Exception ex)
            {

                //throw;
            }

            #region Log
            // Set up a timer that triggers every minute 結束時監控
            //Timer timer = new Timer();
            //timer.Interval = 60000; // 60 seconds
            //timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            //timer.Start();
            #endregion
        }

        public void OnTimer(object sender, ElapsedEventArgs args)
        {
            // TODO: Insert monitoring activities here.
            //eventLog1.WriteEntry("Monitoring the System", EventLogEntryType.Information, eventId++);
        }

        protected override void OnStop()
        {
        }

        private bool CheckSequence(int sid, int nid, int iid)
        {
            return sequenceRepository.Query(null, sid, nid, iid).Count() > 0;         
        }

        #region Send Notify Message
        /// <summary>
        /// 發送Notify (multipart)
        /// </summary>
        /// <param name="MemberID"></param>
        /// <param name="ClientID"></param>
        /// <param name="CallbackUrl"></param>
        /// <param name="Token"></param>
        /// <param name="ClientSecret"></param>
        /// <param name="Message"></param>
        /// <param name="ImageFile"></param>
        /// <param name="ImageUrl"></param>
        /// <param name="ImageThumbnailUrl"></param>
        /// <param name="STKID"></param>
        /// <param name="STKPKGID"></param>
        /// <returns></returns>
        public string SetNotify(int Sid, string Message, string ImageUrl, string Sticker, int Iid, string TokenKey)
        {
            int sid = Convert.ToInt32(Sid);
            var serviceInfo = serviceRepository.GetById(sid);
            var stk = Sticker.Split(',');
            MessageView model = new MessageView
            {
                ClientID = serviceInfo.ClientID,
                CallbackUrl = serviceInfo.CallbackUrl,
                ClientSecret = serviceInfo.ClientSecret,
                Message = Message,
                ImageUrl = ImageUrl,
            };
            if (stk.Count() == 2)
            {
                model.STKID = Convert.ToInt32(stk[0]);
                model.STKPKGID = Convert.ToInt32(stk[1]);
            }

            List<int> delList = new List<int>();

            int res = multipartTest(model, TokenKey);
            if (res == 0)
            {
                interlockRepository.Delete(Iid);
                return "delete";
            }

            return "success";
        }

        /// <summary>
        /// multipart 發送訊息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="token"></param>
        private int multipartTest(MessageView model, string tokenKey)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Message))
                    model.Message = "發送訊息~";
                Dictionary<string, object> d = new Dictionary<string, object>()
                {
                    // message , imageFile ... name is provided by LINE API
                    { "message", model.Message },
                    //{ "imageFile", new FormFile()
                    //    {
                    //        Name = "notify.jpg", ContentType = "image/jpeg", FilePath="notify.jpg"
                    //    }
                    //}
                };
                if (model.STKID != 0 && model.STKPKGID != 0)
                {
                    d.Add("stickerPackageId", model.STKPKGID.ToString());
                    d.Add("stickerId", model.STKID.ToString());
                    //postData += "&stickerPackageId=" + model.STKPKGID.ToString();
                    //postData += "&stickerId=" + model.STKID.ToString();
                }

                if (!string.IsNullOrEmpty(model.ImageFile))
                {
                    //d.Add("imageFile", new FormFile()
                    //{ Name = Path.GetFileName(model.ImageFile), ContentType = "image/jpeg", FilePath = model.ImageFile }
                    //);
                }

                if (!string.IsNullOrEmpty(model.ImageUrl))
                {
                    d.Add("imageFullsize", model.ImageUrl);
                    //postData += "&imageFullsize=" + model.ImageUrl;
                    if (!string.IsNullOrEmpty(model.ImageThumbnailUrl))
                        d.Add("imageThumbnail", model.ImageThumbnailUrl);
                    //postData += "&imageThumbnail=" + model.ImageThumbnailUrl;
                    else
                        d.Add("imageThumbnail", model.ImageUrl);
                    //postData += "&imageThumbnail=" + model.ImageUrl;
                }

                string boundary = "Boundary";
                List<byte[]> output = genMultPart(d, boundary);
                int res = lineNotifyMultipart(tokenKey, boundary, output);
                return res;
            }
            catch (Exception ex)
            {
                return 7;
            }
        }

        private int lineNotifyMultipart(string access_token, string boundary, List<byte[]> output)
        {
            try
            {
                #region POST multipart/form-data
                StringBuilder sb = new StringBuilder();
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(notifyUrl);
                request.Method = "POST";
                request.ContentType = "multipart/form-data; boundary=Boundary";
                request.Timeout = 30000;

                // header
                sb.Clear();
                sb.Append("Bearer ");
                sb.Append(access_token);
                request.Headers.Add("Authorization", sb.ToString());
                // note: multipart/form-data boundary must exist in headers ContentType
                sb.Clear();
                sb.Append("multipart/form-data; boundary=");
                sb.Append(boundary);
                request.ContentType = sb.ToString();

                // write Post Body Message
                BinaryWriter bw = new BinaryWriter(request.GetRequestStream());
                foreach (byte[] bytes in output)
                    bw.Write(bytes);

                #endregion

                int res = getResponse(request, access_token);

                return res;

            }
            catch (Exception ex)
            {
                #region Exception
                StringBuilder sbEx = new StringBuilder();
                sbEx.Append(ex.GetType());
                sbEx.AppendLine();
                sbEx.AppendLine(ex.Message);
                sbEx.AppendLine(ex.StackTrace);
                if (ex.InnerException != null)
                    sbEx.AppendLine(ex.InnerException.Message);
                //myException ex2 = new myException(sbEx.ToString());
                //message(ex2.Message);
                return 6;
                #endregion
            }
        }

        private int getResponse(HttpWebRequest request, string access_token)
        {
            StringBuilder sb = new StringBuilder();
            string result = string.Empty;
            StreamReader sr = null;
            try
            {
                #region Get Response
                if (request == null)
                    return 99;
                // HttpWebRequest GetResponse() if error happened will trigger WebException
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    sb.AppendLine();
                    foreach (var x in response.Headers)
                    {
                        sb.Append(x);
                        sb.Append(" : ");
                        sb.Append(response.Headers[x.ToString()]);
                        if (x.ToString() == "X-RateLimit-Reset")
                        {
                            sb.Append(" ( ");
                            sb.Append(DateTimeOffset.FromFileTime(long.Parse(response.Headers[x.ToString()])));
                            sb.Append(" )");
                        }
                        sb.AppendLine();
                    }
                    using (sr = new StreamReader(response.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                        sb.Append(result);
                    }
                }
                return 1;
                //message(sb.ToString());

                #endregion
            }
            catch (WebException ex)
            {
                if (ex.Message == "遠端伺服器傳回一個錯誤: (401) 未經授權。")
                {
                    //var unLink = interlockRepository.GetAll().Where(q => q.TokenKey == access_token).FirstOrDefault();
                    //interlockRepository.Delete(unLink.ID);
                    return 0;
                }
                else
                {
                    #region WebException handle
                    //var wrong_token = tokenRepository.GetAll().Where(q => q.TokenKey == access_token).FirstOrDefault();
                    //tokenRepository.Delete(wrong_token.ID);

                    // WebException Response
                    using (HttpWebResponse response = (HttpWebResponse)ex.Response)
                    {
                        sb.AppendLine("Error");
                        foreach (var x in response.Headers)
                        {
                            sb.Append(x);
                            sb.Append(" : ");
                            sb.Append(response.Headers[x.ToString()]);
                            sb.AppendLine();
                        }
                        using (sr = new StreamReader(response.GetResponseStream()))
                        {
                            result = sr.ReadToEnd();
                            sb.Append(result);
                        }

                        //message(sb.ToString());
                    }
                    return 5;
                    #endregion
                }
            }
        }

        public List<byte[]> genMultPart(Dictionary<string, object> parameters, string boundary)
        {
            StringBuilder sb = new StringBuilder();
            sb.Clear();
            sb.Append("\r\n--");
            sb.Append(boundary);
            sb.Append("\r\n");
            string beginBoundary = sb.ToString();
            sb.Clear();
            sb.Append("\r\n--");
            sb.Append(boundary);
            sb.Append("--\r\n");
            string endBoundary = sb.ToString();
            sb.Clear();
            sb.Append("Content-Type: multipart/form-data; boundary=");
            sb.Append(boundary);
            sb.Append("\r\n");
            List<byte[]> byteList = new List<byte[]>();
            byteList.Add(System.Text.Encoding.UTF8.GetBytes(sb.ToString()));

            foreach (KeyValuePair<string, object> pair in parameters)
            {
                //if (pair.Value is FormFile)
                //{
                //    byteList.Add(System.Text.Encoding.ASCII.GetBytes(beginBoundary));
                //    FormFile form = pair.Value as FormFile;

                //    sb.Clear();
                //    sb.Append("Content-Disposition: form-data; name=\"");
                //    sb.Append(pair.Key);
                //    sb.Append("\"; filename=\"");
                //    sb.Append(form.Name);
                //    sb.Append("\"\r\nContent-Type: ");
                //    sb.Append(form.ContentType);
                //    sb.Append("\r\n\r\n");
                //    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
                //    byteList.Add(bytes);
                //    if (form.bytes == null && !string.IsNullOrEmpty(form.FilePath))
                //    {
                //        FileStream fs = new FileStream(form.FilePath, FileMode.Open, FileAccess.Read);
                //        MemoryStream ms = new MemoryStream();
                //        fs.CopyTo(ms);
                //        byteList.Add(ms.ToArray());
                //    }
                //    else
                //        byteList.Add(form.bytes);
                //}
                //else
                //{
                    byteList.Add(System.Text.Encoding.ASCII.GetBytes(beginBoundary));
                    sb.Clear();
                    sb.Append("Content-Disposition: form-data; name=\"");
                    sb.Append(pair.Key);
                    sb.Append("\"");
                    sb.Append("\r\n\r\n");
                    sb.Append(pair.Value);
                    string data = sb.ToString();
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(data);
                    byteList.Add(bytes);
                //}
            }

            byteList.Add(System.Text.Encoding.ASCII.GetBytes(endBoundary));
            return byteList;
        }
        #endregion
    }
}
