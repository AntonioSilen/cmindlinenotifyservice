﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CmindLineNotifyLoop.Models
{
    public class MessageView
    {
        public string MID { get; set; }
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
        public string CallbackUrl { get; set; }
        [Display(Name = "訊息")]
        public string Message { get; set; }
        [Display(Name = "貼圖STK ID")]
        public int STKID { get; set; }
        [Display(Name = "貼圖STKPKG ID")]
        public int STKPKGID { get; set; }
        [Display(Name = "本機圖片路徑")]
        public string ImageFile { get; set; }
        [Display(Name = "圖片連結")]
        public string ImageUrl { get; set; }
        [Display(Name = "縮圖連結")]
        public string ImageThumbnailUrl { get; set; }
        public string Token { get; set; }
        public string Code { get; set; }
        public int Type { get; set; }
    }
}
