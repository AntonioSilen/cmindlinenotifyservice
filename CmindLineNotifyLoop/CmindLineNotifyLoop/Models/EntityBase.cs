﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CmindLineNotifyLoop.Models
{
    public class EntityBase
    {
        public virtual int ID { get; set; }

        public virtual int Creater { get; set; }

        public virtual DateTime CreateTime { get; set; }
        public virtual DateTime CreateDate { get; set; }

        public virtual int Updater { get; set; }

        public virtual DateTime UpdateTime { get; set; }
        public virtual DateTime UpdateDate { get; set; }
    }
}
