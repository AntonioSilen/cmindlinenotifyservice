﻿using CmindLineNotifyLoop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CmindLineNotifyLoop.Repositories
{
    public class ServiceRepository : GenericRepository<Service>
    {
        public ServiceRepository(CmindLineNotifyDBEntities context) : base(context) { }
        private CmindLineNotifyDBEntities db = new CmindLineNotifyDBEntities();

        public IQueryable<Service> Query(int vid, bool? active, bool? approval, string searchValue = "", string vendorStr = "", string manageAcc = "")
        {
            var query = GetAll();
            if (vid != 0)
            {
                query = query.Where(p => p.VendorID == vid);
            }
            if (active.HasValue)
            {
                query = query.Where(p => p.IsActive == active);
            }
            if (approval.HasValue)
            {
                query = query.Where(p => p.IsApproval == approval);
            }
            if (!string.IsNullOrEmpty(searchValue))
            {
                query = query.Where(p => p.ServiceName.Contains(searchValue));
            }
            if (!string.IsNullOrEmpty(vendorStr))
            {
                List<int> vendor = db.Vendor.Where(q => q.Name.Contains(vendorStr)).Select(q => q.ID).ToList();
                query = query.Where(q => vendor.Contains(q.VendorID));
            }
            if (!string.IsNullOrEmpty(manageAcc))
            {
                
                var acc = db.ManageAccount.Where(q => q.Status == true && q.Account == manageAcc).FirstOrDefault();
                if (acc != null)
                {
                    if (!string.IsNullOrEmpty(acc.Services))
                    {
                        List<int> intIdList = new List<int>();
                        var sidList = acc.Services.Split(',');
                        foreach (var srvs in sidList)
                        {
                            if (!string.IsNullOrEmpty(srvs))
                            {
                                intIdList.Add(Convert.ToInt32(srvs));
                            }
                        }
                        query = query.Where(q => intIdList.Contains(q.ID));
                    }
                }
            }

            return query;
        }
    }
}