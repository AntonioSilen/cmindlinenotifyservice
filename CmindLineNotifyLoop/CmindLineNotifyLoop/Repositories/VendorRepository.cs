﻿using CmindLineNotifyLoop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CmindLineNotifyLoop.Repositories
{
    public class VendorRepository : GenericRepository<Vendor>
    {
        public VendorRepository(CmindLineNotifyDBEntities context) : base(context) { }
        private CmindLineNotifyDBEntities db = new CmindLineNotifyDBEntities();

        public IQueryable<Vendor> Query(bool? active, bool? approval, string name, string email, string lineId = "")
        {
            var query = GetAll();
            if (active.HasValue)
            {
                query = query.Where(p => p.IsActive == active);
            }
            if (approval.HasValue)
            {
                query = query.Where(p => p.IsActive == approval);
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.Name.Contains(name));
            }
            if (!string.IsNullOrEmpty(email))
            {
                query = query.Where(p => p.Email.Contains(email));
            }
            if (!string.IsNullOrEmpty(lineId))
            {
                query = query.Where(q => q.LineID == lineId);
            }

            return query;
        }
    }
}