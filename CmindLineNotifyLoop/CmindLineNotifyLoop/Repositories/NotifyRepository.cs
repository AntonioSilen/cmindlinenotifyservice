﻿using CmindLineNotifyLoop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CmindLineNotifyLoop.Repositories
{
    public class NotifyRepository : GenericRepository<NotifyMessage>
    {
        public NotifyRepository(CmindLineNotifyDBEntities context) : base(context) { }
        private CmindLineNotifyDBEntities db = new CmindLineNotifyDBEntities();

        public IQueryable<NotifyMessage> Query(string message, int sid = 0)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(message))
            {
                query = query.Where(p => p.Message.Contains(message));
            }
            if (sid != 0)
            {
                query = query.Where(q => q.ServiceID == sid);
            }

            return query;
        }

        public string DeleteImage(int id)
        {
            NotifyMessage image = db.NotifyMessage.Find(id);
            var delete = image.ImageUrl;
            image.ImageUrl = string.Empty;
            db.SaveChanges();
            return delete;
        }   
    }
}