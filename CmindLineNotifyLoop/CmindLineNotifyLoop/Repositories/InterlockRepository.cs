﻿using CmindLineNotifyLoop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CmindLineNotifyLoop.Repositories
{
    public class InterlockRepository : GenericRepository<Interlock>
    {
        public InterlockRepository(CmindLineNotifyDBEntities context) : base(context) { }
        private CmindLineNotifyDBEntities db = new CmindLineNotifyDBEntities();

        public IQueryable<Interlock> Query(int serviceId)
        {
            var query = GetAll();
            if (serviceId != 0)
            {
                query = query.Where(q => q.ServiceID == serviceId);
            }

            return query;
        }       
    }
}