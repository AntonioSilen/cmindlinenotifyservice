﻿using CmindLineNotifyLoop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CmindLineNotifyLoop.Repositories
{
     public class SequenceRepository : GenericRepository<SendSequence>
    {
        public SequenceRepository(CmindLineNotifyDBEntities context) : base(context) { }
        private CmindLineNotifyDBEntities db = new CmindLineNotifyDBEntities();

        public IQueryable<SendSequence> Query(bool? isSent, int sid = 0, int nid = 0, int iid = 0)
        {
            var query = GetAll();
         
            if (isSent != null)
            {
                query = query.Where(p => p.IsSent == isSent);
            }
            if (sid != 0)
            {
                query = query.Where(p => p.ServiceID == sid);
            }
            if (nid != 0)
            {
                query = query.Where(p => p.NotifyID == nid);
            }
            if (iid != 0)
            {
                query = query.Where(p => p.InterlockID == iid);
            }

            return query.OrderBy(q => q.CreateDate);
        }

        public SendSequence GetByGuid(Guid id)
        {
            return db.SendSequence.Where(q => q.ID == id).FirstOrDefault();           
        }

        public void DeleteByGuid(Guid id)
        {
            var query = db.SendSequence.Where(q => q.ID == id).FirstOrDefault();
            db.SendSequence.Remove(query);
            db.SaveChanges();
        }
    }
}
