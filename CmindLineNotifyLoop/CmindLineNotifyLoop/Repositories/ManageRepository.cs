﻿using CmindLineNotifyLoop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CmindLineNotifyLoop.Repositories
{
    public class ManageRepository : GenericRepository<ManageAccount>
    {
        private const string CMIND_Vendor = "CmindVendorInfo";
        public ManageRepository(CmindLineNotifyDBEntities context) : base(context) { }
        private CmindLineNotifyDBEntities db = new CmindLineNotifyDBEntities();

        public IQueryable<ManageAccount> Query(int vid, bool? active, string searchValue = "", string vendorStr = "")
        {
            var query = GetAll();
            if (vid != 0)
            {
                query = query.Where(p => p.VendorID == vid);
            }
            if (active.HasValue)
            {
                query = query.Where(p => p.Status == active);
            }
            if (!string.IsNullOrEmpty(searchValue))
            {
                query = query.Where(p => p.Account.Contains(searchValue));
            }
            if (!string.IsNullOrEmpty(vendorStr))
            {
                List<int> vendor = db.Vendor.Where(q => q.Name.Contains(vendorStr)).Select(q => q.ID).ToList();
                query = query.Where(q => vendor.Contains(q.VendorID));
            }

            return query;
        }

        //public int ManageLogin(string acc, string pwd)
        //{
        //    var query = GetAll();
        //    if (!string.IsNullOrEmpty(acc) && !string.IsNullOrEmpty(pwd))
        //    {               
        //        var accdata = query.Where(q => q.Account == acc && q.Password == pwd).FirstOrDefault();
        //        if (accdata != null)
        //        {
        //            if (accdata.Status)
        //            {
        //                var vendorInfo = db.Vendor.Where(q => q.ID == accdata.VendorID).FirstOrDefault();
        //                LineUserProfile user = new LineUserProfile();
        //                user.userId = vendorInfo.LineID;
        //                user.displayName = vendorInfo.Name;
        //                user.pictureUrl = vendorInfo.PictureUrl;
        //                user.email = vendorInfo.Email;
        //                user.AccClass = "2";
        //                user.Account = acc;

        //                string data = JsonConvert.SerializeObject(user, new JsonSerializerSettings() { StringEscapeHandling = StringEscapeHandling.EscapeNonAscii });

        //                DateTime expires = DateTime.Now.AddDays(30);
        //                CookieHelper.SetCookie(CMIND_Vendor, data, expires);
        //                return 1;
        //            }
        //            else
        //            {
        //                return 2;
        //            }
        //        }
        //        else
        //        {
        //            return 3;
        //        }
        //    }
        //    return 4;
        //}
    }
}