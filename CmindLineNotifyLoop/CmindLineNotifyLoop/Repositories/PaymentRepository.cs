﻿using CmindLineNotifyLoop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CmindLineNotifyLoop.Repositories
{
    public class PaymentRepository : GenericRepository<PaymentRecord>
    {
        public PaymentRepository(CmindLineNotifyDBEntities context) : base(context) { }
        private CmindLineNotifyDBEntities db = new CmindLineNotifyDBEntities();

        public IQueryable<PaymentRecord> Query(int sid, int status, int type, int plan)
        {
            var query = GetAll();
            if (sid != 0)
            {
                query = query.Where(p => p.ServiceID == sid);
            }
            if (status != 0)
            {
                query = query.Where(p => p.Status == status);
            }
            if (type != 0)
            {
                query = query.Where(p => p.PaymentType == type);
            }
            if (plan != 0)
            {
                query = query.Where(p => p.Plan == plan);
            }

            return query;
        }
    }
}